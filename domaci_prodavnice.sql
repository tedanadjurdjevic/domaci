-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: domaci_prodavnice
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drzava` (
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES ('DEU','Nemacka'),('FRA','Francuska'),('JPN','Japan'),('SRB','Srbija');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grad`
--

DROP TABLE IF EXISTS `grad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pttBroj` varchar(10) DEFAULT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  `drzavaid` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grad_1_idx` (`drzavaid`),
  CONSTRAINT `fk_grad_1` FOREIGN KEY (`drzavaid`) REFERENCES `drzava` (`kod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grad`
--

LOCK TABLES `grad` WRITE;
/*!40000 ALTER TABLE `grad` DISABLE KEYS */;
INSERT INTO `grad` VALUES (1,'11000','Beograd','SRB'),(2,'10115','Berlin','DEU'),(3,'75001','Pariz','FRA'),(4,'1000001','Tokio','JPN');
/*!40000 ALTER TABLE `grad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `kategorijaid` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kategorijaid`)
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (401,'Racunari'),(402,'Elektronika'),(403,'Kucni aparati'),(404,'Video igre');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica` (
  `prodavnicaid` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `gradid` int(11) DEFAULT NULL,
  PRIMARY KEY (`prodavnicaid`),
  KEY `fk_prodavnica_1_idx` (`gradid`),
  CONSTRAINT `fk_prodavnica_1` FOREIGN KEY (`gradid`) REFERENCES `grad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (101,'MegaMarket',1),(102,'SuperStore',2),(103,'Le Merche',3),(104,'TokyoMart',4);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `proizvodid` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `barkod` varchar(13) DEFAULT NULL,
  `proizvodjacid` int(11) DEFAULT NULL,
  `kategorijaid` int(11) DEFAULT NULL,
  PRIMARY KEY (`proizvodid`),
  KEY `fk_proizvod_1_idx` (`proizvodjacid`),
  KEY `fk_proizvod_2_idx` (`kategorijaid`),
  CONSTRAINT `fk_proizvod_1` FOREIGN KEY (`proizvodjacid`) REFERENCES `proizvodjac` (`proizvodjacid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_2` FOREIGN KEY (`kategorijaid`) REFERENCES `kategorija` (`kategorijaid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (201,'Laptop','1234567890123',301,401),(202,'Telefon','2345678901234',302,402),(203,'HD Televizor','3456789012345',303,403),(204,'Konzola za igre','4567890123456',304,404);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod_prodavnica`
--

DROP TABLE IF EXISTS `proizvod_prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod_prodavnica` (
  `idpp` int(11) NOT NULL AUTO_INCREMENT,
  `proizvodid` int(11) NOT NULL,
  `prodavnicaid` int(11) NOT NULL,
  `cena` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`idpp`),
  KEY `fk_proizvod_prodavnica_1_idx` (`prodavnicaid`),
  KEY `fk_proizvod_prodavnica_2_idx` (`proizvodid`),
  CONSTRAINT `fk_proizvod_prodavnica_1` FOREIGN KEY (`prodavnicaid`) REFERENCES `prodavnica` (`prodavnicaid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_prodavnica_2` FOREIGN KEY (`proizvodid`) REFERENCES `proizvod` (`proizvodid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=505 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod_prodavnica`
--

LOCK TABLES `proizvod_prodavnica` WRITE;
/*!40000 ALTER TABLE `proizvod_prodavnica` DISABLE KEYS */;
INSERT INTO `proizvod_prodavnica` VALUES (501,201,101,1200.50),(502,202,102,899.99),(503,203,103,2499.00),(504,204,104,349.99);
/*!40000 ALTER TABLE `proizvod_prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvodjac` (
  `proizvodjacid` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `gradID` int(11) NOT NULL,
  PRIMARY KEY (`proizvodjacid`),
  KEY `fk_proizvodjac_1_idx` (`gradID`),
  CONSTRAINT `fk_proizvodjac_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=305 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (301,'TechGuru',1),(302,'GmbH',2),(303,'High Tech Industries',3),(304,'Sunrise',4);
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-23 15:10:18
